UPDATE `modxdb`.`jal6F32Lkfod_localizator_languages` SET `http_host` = 'EXTERNAL_HOSTNAME/ru/' WHERE (`id` = '1');
UPDATE `modxdb`.`jal6F32Lkfod_localizator_languages` SET `http_host` = 'EXTERNAL_HOSTNAME' WHERE (`id` = '2');
UPDATE `modxdb`.`jal6F32Lkfod_localizator_languages` SET `http_host` = 'EXTERNAL_HOSTNAME/es/' WHERE (`id` = '3');
UPDATE `modxdb`.`jal6F32Lkfod_localizator_languages` SET `http_host` = 'EXTERNAL_HOSTNAME/de/' WHERE (`id` = '4');
UPDATE `modxdb`.`jal6F32Lkfod_localizator_languages` SET `http_host` = 'EXTERNAL_HOSTNAME/it/' WHERE (`id` = '5');
UPDATE `modxdb`.`jal6F32Lkfod_context_setting` SET `value` = 'https://EXTERNAL_HOSTNAME/' WHERE (`context_key` = 'mgr') and (`key` = 'site_url');
UPDATE `modxdb`.`jal6F32Lkfod_context_setting` SET `value` = 'https://EXTERNAL_HOSTNAME/' WHERE (`context_key` = 'web') and (`key` = 'site_url');
UPDATE `modxdb`.`jal6F32Lkfod_system_settings` SET `value` = 'https' WHERE (`key` = 'server_protocol');

UPDATE `modxdb`.`jal6F32Lkfod_system_settings` SET `value` = 'https://LK_HOSTNAME/login.aspx' WHERE (`key` = 'login_link');
UPDATE `modxdb`.`jal6F32Lkfod_system_settings` SET `value` = 'https://LK_HOSTNAME/create_account.aspx' WHERE (`key` = 'register_link');
UPDATE `modxdb`.`jal6F32Lkfod_system_settings` SET `value` = 'https://LK_HOSTNAME/Handler.ashx?func=loan_stats' WHERE (`key` = 'stat_overall_url');
UPDATE `modxdb`.`jal6F32Lkfod_system_settings` SET `value` = 'https://LK_HOSTNAME/Handler.ashx?func=mfc_loan_list' WHERE (`key` = 'stat_primary_market_url');
UPDATE `modxdb`.`jal6F32Lkfod_system_settings` SET `value` = 'https://LK_HOSTNAME/affiliate_signin.aspx' WHERE (`key` = 'affiliate_signin');
UPDATE `modxdb`.`jal6F32Lkfod_system_settings` SET `value` = 'en' WHERE (`key` = 'localizator_default_language');
UPDATE `modxdb`.`jal6F32Lkfod_system_settings` SET `value` = '1' WHERE (`key` = 'pdotools_fenom_cache');
UPDATE `modxdb`.`jal6F32Lkfod_system_settings` SET `value` = '.DOMAIN' WHERE (`key` = 'cookies_domain');
