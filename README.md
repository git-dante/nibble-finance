
###Deploy
___
Before deploy check/setup environments in:
 * docker-compose.yml
 * ./init_db/server_url.sql
 * ./prod_update_db.sql (for production Environment)

For run service
```
docker-compose up -d
```
For remove service
```
docker-compose down
```